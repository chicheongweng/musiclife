
 
日期：  2015年8月7日 17:50:56 --- V1.4.1 released for both HD and XS production.
1_ Bull foot LED off when unit is off. Modified to better breathing (both blue and white LED). Decrease white LED from 0x800 to 0x600. (a little bit darker)



2_ Jingle volume to DSP set to fixed value (level 16), while jingle sounds were modified, -20dB down for HD and Bull, -12dB down for XS. Total jingle data size got decreased from 5xxk to 4xxk byte.



3_ XS dsp firmware BIN updated to the new version from YuFei (May 27th version).



4_ Modified PS data file (used when building DFU) to have DSP/MFI version record deleted. So that the DFU built and the final AeroUpdate.exe would have CSR program the SPI flash of DSP for sure.



5_ When CSR is updating DSP/MFI, AMP IC is powered down (ShutDown pin set to L, before was H). This avoids noise during updating DSP. Also when powering down, changed to power down AMP IC first, then power down DSP.



6_ On XS, added low battery jingle warning when there is DC plugged and powered on, also with red LED blinking. Jingle warning comes out once every minute. The warning jingle happens in 5 mimutes (low battery status lasts for about 5 minutes), before it is auto shut down.



7_ NFC problem fixed on HD, by adding one more I2C command to the NFC chip, a deselecting command, to let the NFC chip be in a certain and correct mode/state.



8_ Fixed XS problem: searching for the root cause was difficult, but the fix is simple: old code doesn’t set new DSP status to OFF when it is turning off the DSP. Just added a line to set the new DSP status to OFF. All the 3 problems fixed.
 a/ powered off, plug dc, unplug dc, power on  no power up, no sound (DSP not ON) --- --- fixed!

 b/ powered off, first no DC plugged, then plug dc  will trigger CSR updates DSP. --- --- fixed! 
 
 c/ powered off, no DC plugged, wait 30s, plug DC  will immediately trigger CSR updates DSP. --- --- fixed!
 


9_ TWS enabled on HD, but only for blue tooth A2DP streaming. Not OK for line in. Changed PS data file (used when building DFU) to have TWS source be A2DP, instead of original AUTO. (so, appearently only A2DP works in TWS). To have better TWS stable, increase heartbeat cycle time from 200ms to 250ms, increased LED update cycle time from 120ms to 200ms.



10_ Fixed iPhone volume problem. in Sink_Avrcp.c, modified source code from "/127" to "/128". Also volume curved changed.



11_ Removed the battery low jingle when DC is plugged/



12_ Disconnect all before TWS pairing.(But if this kills the super long time paring problem is unknown, needs more testing ... )



13_ HD Series, changed to: NOT auto power on after initialization is finished. So, plug DC will not turn on the device. Only you press the power button, you turn on the device.



14_ TWS: after TWS is paired, force both master and slave unit into pairing mode. (trigger a user pair event)

15_ V1.4.1 for Both XS and HD. (for HD, Mfi audio is 8dB decreased, this is to prevent overload power off.)


Still need to check paring problem, sometimes the units seems to be hidden, can not be paired with cell phone, can not be TWS paried with anther unit.






2015-02-07: AeroUpdate-1.3.7.exe, Aerobull-1.3.7.zip

This release fixes the jingle volume problem on the Bull; it also
contains the latest firmware for the iPhone dock chip.

2015-02-16: 
Added AeroUpdate-1.3.8.exe: In this release the gain curve for Bull and HD is made smoother (was a bit too steep in the first half) and the max is reduced by 5dB.)
Added AeroUpdate-1.3.14.exe: fix for remote control issue (AB-24)

2015-03-09:
Added AeroUpdate-1.3.15.exe: This release has 01_SkullHD 2210 V8AC.preset and 01_Bull - Ivan VS HD bass Jan17 compressor-6dB limiter 0dB 1500Hz lower 2dB for Marc Feb12-4.preset. Interally, alldsp label those preset data as HD 2210 V8AC per Mar 6, 2015 email.

2015-03-13:
Added AeroUpdate-1.3.2-1fff.exe for AeroSkull XS.

2015-03-13:
Added AeroUpdate-1.3.16.exe for AeroSkull HD and Bull.

2015-03-30:
Added image files xs_1.3.2_dump.zip for AeroUpdate-1.3.2-1fff.exe

2015-03-31:
Added AeroUpdate-1.3.18.exe (confirmed NFC fix for AeroSkull HD)

2015-04-09:
Added AeroSkullNano_1750.bin (currently in production)

2015-04-13:
Cannot use 1.3.18 because due to the volume jump killing the PCB. 1.3.16 does not have the problem because it automatically cuts off the current. Tested 1.3.21 and the result is good (NFC + Bluetooth pairing + acoustics).

2015-04-24:
Added AeroUpdate-1.3.25.exe, which has the TWS fix

2015-04-27:
Added AeroUpdate-1.3.25 firmware

2015-05-26: added AeroSkullHD.ffi with 01_HD 2210 V8AC output balanced compressor Feb23 EQ4.preset

2015-05-28: added AeroSkull_XS_1.4.0.ffi, AeroSkull_XS_1.4.0.exe, and AeroSkull_XS_1.4.0_...preset

2015-05-29: Ivan's rrr file was an rar archive. So I am replacing AeroSkull-XS-1.4.0.exe back with Ivan's test file.

2015-05-29: add xpv/xdv dump for AeroUpdate-XS-1.4.0
