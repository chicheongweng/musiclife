2015-02-16:

01_Bull - Ivan VS HD bass Jan17 compressor-6dB limiter 0dB 1500Hz lower 2dB for Marc Feb12-4.preset: approved by Marc for AeroBull HD 

2015-02-16:

01_Bull - Ivan VS HD bass Jan17 compressor-15dB limiter 0dB for Roland.preset: current production

01_SkullHD 2210 V8AC.preset: current production

01_SkullXS_Alain_preset_2014.preset: current production

2015-03-09: added 00_Bull_Yufei_50307.preset to be packaged in 1.3.16.exe

2015-03-26: added 01_HD 2210 V8AC output balanced compressor Feb23 EQ4.preset to be packaged in AeroUpdate-1.3.26.exe (skull preset)
